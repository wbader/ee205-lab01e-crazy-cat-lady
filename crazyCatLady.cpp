///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01e - Crazy Cat Lady - EE 205 - Spr 2022
///
/// @file    crazyCatLady.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o crazyCatLady crazyCatLady.cpp
///
/// Usage:  crazyCatLady catName
///
/// Result:
///   Oooooh! [catName] you’re so cute!
///
/// Example:
///   $ ./crazyCatLady Snuggles
///   Oooooh! Snuggles you’re so cute!
///
/// @author  Waylon Bader <wbadeR@hawaii.edu>
/// @date    20 Jan 2021 
///////////////////////////////////////////////////////////////////////////////


#include <iostream>

int main( int argc, char* argv[] ) {
//	std::cout << "Crazy Cat Lady!" << std::endl ;
  

   std::cout << "Oooooooh! " << argv[1] << " you're so cute!" << std::endl;

   return 0;
}
